# Remote-display-client-benchmark

Playground for experimenting with the sending of real time display updates over the wire.

## Problem statement

A remote display requires quite a large amount of data.

```
 Example:
 screen_width: 256px
 screen_height: 128px
 screen_coordinate_datatype: 32bit
 color_depth: 16bit
 json_overhead: ~10%

 coordinate_bytes = screen_width * screen_height * screen_coordinate_datatype
 color_bytes = screen_width * screen_height * color_depth
 full_screen_render_bytes = coordinate_bytes + color_bytes + 10%
 1 572 864 bytes = 1.5MB
```

As you can see, rendering a display over the wire is no small matter if it has to be done
within highly confined resource requirements and deadlines. This project is intended as a
playground for experiementing with remote display performance issues.

## Authors and acknowledgment

### External libraries

Thanks to all the amazing people maintaining c libraries such as:

* https://github.com/laurencelundblade/QCBOR


## Requirements

* PlatformIO
* ESP32 Microcontroller

## Running

This repository uses git submodules
```
git submodule init
git submodule update --recursive
```

### Configure remote endpoints

Configuration is done with menuconfig.
```
platformio run --target menuconfig
```

Required configuration to be done:
* Wifi
* Benchmarks

Since benchmarks can take a very long time, they are disabled by default and need to explicitly be enabled in the config. Remote endpoints mqtt/http can also be found there.

## Flashing

To keep the resource constraints as close to the real deal as possible, this project is being flashed on to an ESP32 via PlatformIO.

```sh
platformio run --target upload --target monitor
```

## Demo

```txt
␛[0;32mI (792) wifi: WIFI station connected!
␛[0m
W (802) wifi:<ba-add>idx:0 (ifx:0, 60:38:e0:c8:45:d1), tid:6, ssn:2, winSize:64
I (802) wifi:AP's beacon interval = 102400 us, DTIM period = 3
␛[0;32mI (2032) esp_netif_handlers: sta ip: 192.168.137.179, mask: 255.255.255.0, gw: 192.168.137.1␛[0m
␛[0;32mI (2032) wifi: Received station address.
␛[0m
␛[0;32mI (2032) wifi: IP:192.168.137.179
...
␛[0;32mI (20172) cbor_publisher: Encoded buffer of size 618: ␛[0m
␛[0;32mI (20472) cbor_publisher: Encoded buffer of size 618: ␛[0m
␛[0;32mI (20472) cbor_publisher: Encoded buffer of size 618: ␛[0m

␛[0;32mI (20472) _start_benchmarks: Execution of {
    cbor_publisher__publish(128, 160);
} took 15.850000 seconds.
␛[0m

␛[0;32mI (34402) _start_benchmarks: Execution of {
    json_publisher__publish(128, 160);
} took 13.920000 seconds.
```