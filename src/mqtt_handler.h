#pragma once
#include "mqtt_facade.h"

void mqtt_handler__initialize(esp_mqtt_client_config_t client_config);
void mqtt_handler__start();