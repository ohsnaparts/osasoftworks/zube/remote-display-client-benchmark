#include "mqtt_facade.h"
#include "esp_log.h"

static const char *TAG = "mqtt_handler";
static esp_mqtt_client_handle_t _client;

static void (*_handle_mqtt_connected_fn)(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event) = NULL;
static void (*_handle_mqtt_subscribed_fn)(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event) = NULL;
static void (*_handle_mqtt_disconnected_fn)(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event) = NULL;
static void (*_handle_mqtt_error_fn)(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event) = NULL;
static void (*_handle_mqtt_fallback_fn)(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event) = NULL;
static void (*_handle_mqtt_data_received_fn)(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event) = NULL;
static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    esp_mqtt_event_handle_t event = event_data;
    esp_mqtt_client_handle_t client = event->client;

    switch ((esp_mqtt_event_id_t)event_id)
    {
    case MQTT_EVENT_CONNECTED:
        if (_handle_mqtt_connected_fn != NULL)
        {
            _handle_mqtt_connected_fn(&client, &event);
        }
        break;
    case MQTT_EVENT_SUBSCRIBED:
        if (_handle_mqtt_subscribed_fn != NULL)
        {
            _handle_mqtt_subscribed_fn(&client, &event);
        }
        break;
    case MQTT_EVENT_DATA:
        if (_handle_mqtt_data_received_fn != NULL) {
            _handle_mqtt_data_received_fn(&client, &event);
        }
        break;
    case MQTT_EVENT_DISCONNECTED:
        if (_handle_mqtt_disconnected_fn != NULL)
        {
            _handle_mqtt_disconnected_fn(&client, &event);
        }
        break;
    case MQTT_EVENT_ERROR:
        if (_handle_mqtt_error_fn != NULL)
        {
            _handle_mqtt_error_fn(&client, &event);
        }
        break;
    default:
        if (_handle_mqtt_fallback_fn != NULL)
        {
            _handle_mqtt_fallback_fn(&client, &event);
        }
        break;
    }
}

void mqtt_facade__initialize(
    esp_mqtt_client_config_t mqtt_config,
    void (*handle_mqtt_connected_fn)(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event),
    void (*handle_mqtt_subscribed_fn)(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event),
    void (*handle_mqtt_disconnected_fn)(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event),
    void (*handle_mqtt_error_fn)(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event),
    void (*handle_mqtt_fallback_fn)(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event),
    void (*handle_mqtt_data_received_fn)(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event))
{
    _client = esp_mqtt_client_init(&mqtt_config);
    
    _handle_mqtt_connected_fn = handle_mqtt_connected_fn;
    _handle_mqtt_subscribed_fn = handle_mqtt_subscribed_fn;
    _handle_mqtt_disconnected_fn = handle_mqtt_disconnected_fn;
    _handle_mqtt_error_fn = handle_mqtt_error_fn;
    _handle_mqtt_fallback_fn = handle_mqtt_fallback_fn;
    _handle_mqtt_data_received_fn = handle_mqtt_data_received_fn;
    /* The last argument may be used to pass data to the event handler, in this example mqtt_event_handler */
    esp_mqtt_client_register_event(_client, ESP_EVENT_ANY_ID, mqtt_event_handler, NULL);
}

void mqtt_facade__subscribe(const char *topic)
{
    int msg_id = esp_mqtt_client_subscribe(_client, topic, 0);
    ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);
}

void mqtt_facade__publish(const char *topic, const char *message, uint16_t message_length)
{
    esp_mqtt_client_publish(_client, topic, message, message_length, 0, 0);
}

void mqtt_facade__start()
{
    esp_mqtt_client_start(_client);
}