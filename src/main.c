#include "stdio.h"
#include "measure.h"
#include "wifi_setup.h"
#include "mqtt_handler.h"
#include "delay_helper.h"

void app_main()
{
    MEASURE({
        create_wifi_station();
        mqtt_handler__initialize((esp_mqtt_client_config_t) {
            .uri = CONFIG_MQTT_BROKER_URL,
            .out_buffer_size = CONFIG_MQTT_OUTBOX_BUFFER_SIZE
        });
    });
    
    mqtt_handler__start();

    while (1)
    {
        printf(".");
        delay_helper__delay(10000);
    }
}
