#include "mqtt_handler.h"
#include "json_publisher.h"
#include "cbor_publisher.h"
#include "measure.h"

static const char *TAG = "mqtt_handler";

static void _start_benchmarks()
{
#if CONFIG_BENCHMARK_CBOR_ENABLED
    MEASURE({
        cbor_publisher__publish(128, 160);
    });
#endif
#if CONFIG_BENCHMARK_JSON_ENABLED
    MEASURE({
        json_publisher__publish(128, 160);
    });
#endif
}

static void _handle_mqtt_connected(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event)
{
    ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
    mqtt_facade__subscribe(CONFIG_MQTT_TOPIC_DATA);
}

static void _handle_mqtt_subscribed(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event)
{
    _start_benchmarks();
}

static void _handle_mqtt_disconnected(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event)
{
    ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
}

static void _handle_mqtt_error(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event)
{
    ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
    bool is_transport_error = (*event)->error_handle->error_type == MQTT_ERROR_TYPE_TCP_TRANSPORT;
    if (is_transport_error)
    {
        esp_mqtt_error_codes_t *error = (*event)->error_handle;
        ESP_LOGE(TAG, "Last errno string (%s)", strerror(error->esp_transport_sock_errno));
    }
}

static void _handle_mqtt_fallback(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event)
{
    ESP_LOGI(TAG, "Other event id:%d", (*event)->event_id);
}

static void _handle_data_received(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event)
{
    printf("Received %.*s: %.*s\r\n", (*event)->topic_len, (*event)->topic, (*event)->data_len, (*event)->data);
}

void mqtt_handler__initialize(esp_mqtt_client_config_t client_config)
{
    cbor_publisher__initialize();
    mqtt_facade__initialize(
        client_config,
        _handle_mqtt_connected,
        _handle_mqtt_subscribed,
        _handle_mqtt_disconnected,
        _handle_mqtt_error,
        _handle_mqtt_fallback,
        _handle_data_received);
}

void mqtt_handler__start()
{
    mqtt_facade__start();
}