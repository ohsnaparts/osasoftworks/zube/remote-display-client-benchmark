#include "json_publisher.h"
#include "cJSON.h"
#include "mqtt_facade.h"

static cJSON *pixel_array;


static void dispose_pixel_array() {
    cJSON_Delete(pixel_array);
    pixel_array = NULL;
}

static void start_chunk() {
    dispose_pixel_array();
    pixel_array = cJSON_CreateArray();
}

static void add_to_message_chunk(uint8_t x, uint8_t y) {
    cJSON* pixel = cJSON_CreateArray();
    cJSON_AddItemToArray(pixel, cJSON_CreateNumber(x));
    cJSON_AddItemToArray(pixel, cJSON_CreateNumber(y));
    cJSON_AddItemToArray(pixel_array, pixel);
}

// Mind that QoS above 0 causes significant load on the little ESP device.
// With the DevKit 4, the backpressure just gets too great, causing the outbox
// buffer to overflow. Not to mention the increase in time due to waiting for
// acknowlegements
static void flush_message() {
    const char* json_payload = cJSON_PrintUnformatted(pixel_array);
    
    mqtt_facade__publish(CONFIG_MQTT_TOPIC_COMMAND, (char *)json_payload, strlen(json_payload));
    
    free((void*)json_payload);
}

void json_publisher__publish(
    uint8_t screen_width,
    uint8_t screen_height
) {    
    for(uint8_t x = 0; x < screen_width; x++) {
        start_chunk();
        for(uint8_t y = 0; y < screen_height; y++) {
            add_to_message_chunk(x, y);
        }
        // flush vertical pixel column
        flush_message();
    }
}
