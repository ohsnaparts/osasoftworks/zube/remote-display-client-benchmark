#pragma once
#include "mqtt_client.h"

void mqtt_facade__initialize(
    esp_mqtt_client_config_t mqtt_cfg,
    void (*_handle_mqtt_connected_fn)(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event),
    void (*_handle_mqtt_subscribed_fn)(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event),
    void (*_handle_mqtt_disconnected_fn)(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event),
    void (*_handle_mqtt_error_fn)(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event),
    void (*_handle_mqtt_fallback_fn)(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event),
    void (*_handle_mqtt_data_received_fn)(esp_mqtt_client_handle_t *client, esp_mqtt_event_handle_t *event)
);

void mqtt_facade__subscribe(const char *topic);
void mqtt_facade__publish(const char *topic, const char* message, uint16_t message_length);
void mqtt_facade__start();