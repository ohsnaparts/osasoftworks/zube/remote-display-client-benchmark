#include "cbor_publisher.h"
#include "cbor.h"
#include "esp_log.h"
#include "mqtt_facade.h"

static uint8_t buf[1024];
static CborEncoder root_encoder;
static CborEncoder chunk_encoder;

static void start_chunk()
{
    cbor_encoder_create_array(&root_encoder, &chunk_encoder, CborIndefiniteLength);
}

static void add_to_message_chunk(uint8_t x, uint8_t y)
{
    CborEncoder pixel_tupple;
    cbor_encoder_create_array(&chunk_encoder, &pixel_tupple, 2);
    cbor_encode_uint(&pixel_tupple, x);
    cbor_encode_uint(&pixel_tupple, y);
    cbor_encoder_close_container(&chunk_encoder, &pixel_tupple);
}

static void flush_chunk()
{
    size_t buffer_size = cbor_encoder_get_buffer_size(&root_encoder, buf);
    if (buffer_size == 0)
    {
        ESP_LOGI("flush_chunk", "Nothing to flush. Skipping...");
        return;
    }

    uint8_t *buffer = _cbor_encoder_get_buffer_pointer(&root_encoder);
    ESP_LOGI("cbor_publisher", "Encoded buffer of size %d: %s", buffer_size, buffer);
    mqtt_facade__publish(CONFIG_MQTT_TOPIC_COMMAND, (char *)buffer, buffer_size);

    // reset buffer
    cbor_publisher__initialize();
}

static void close_chunk()
{
    cbor_encoder_close_container(&root_encoder, &chunk_encoder);
    flush_chunk();
}

void cbor_publisher__initialize()
{
    cbor_encoder_init(&root_encoder, buf, sizeof(buf), 0);
    memset(buf, 0, sizeof(buf));
}

void cbor_publisher__publish(uint8_t screen_width, uint8_t screen_height)
{
    for (uint8_t y = 0; y < screen_height; y++)
    {
        start_chunk();
        for (uint8_t x = 0; x < screen_width; x++)
        {
            add_to_message_chunk(x, y);
        }
        // flush vertical pixel column
        close_chunk();
    }
}