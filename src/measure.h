#pragma once
#include "time.h"
#include "esp_log.h"


#define MEASURE(code_block)                                                                  \
    do                                                                                       \
    {                                                                                        \
        clock_t clock_begin = clock();                                                       \
        do                                                                                   \
            code_block while (0);                                                            \
        clock_t clock_end = clock();                                                         \
        double elapsed_time = (double)(clock_end - clock_begin) / CLOCKS_PER_SEC;            \
        ESP_LOGI(__func__, "Execution of %s took %f seconds.\n", #code_block, elapsed_time); \
    } while (0);

// eof

