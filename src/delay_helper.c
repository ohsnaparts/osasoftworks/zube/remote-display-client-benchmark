#include "delay_helper.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

void delay_helper__delay(uint16_t milliseconds) {
    vTaskDelay(milliseconds / portTICK_RATE_MS);
}