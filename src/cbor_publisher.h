#pragma once
#include "stdint.h"
#include "mqtt_client.h"

void cbor_publisher__initialize();
void cbor_publisher__publish(
    uint8_t screen_width,
    uint8_t screen_height
);